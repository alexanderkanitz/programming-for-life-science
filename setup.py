from setuptools import setup, find_packages

setup(
    name='transcript_sampler_unibas',
    url='https://gitlab.com/garion0000/programming-for-life-science',
    author='Michele Garioni',
    author_email='michele.garioni@unibas.ch',
    description='A package that defines a simple transcript sampler',
    license='MIT',
    version='1.0.0',
    packages=find_packages(),  # this will autodetect Python packages from the directory tree, e.g., in `code/`
    install_requires=['random','argparse'],  # add here packages that are required for your package to run, including version or range of versions
    entry_points={'console_scripts': ['sample_as=transcript_sampler_unibas.cli:main']}
)