from random import choices

class TranscriptSampler:

    def read_avg_expression(self,file):
        myfile=open(file,'r')
        #initialize empty dictionary
        dc={}
        #read line and split key-value
        for myline in myfile:
            d=myline.split(' ')
            #assing key and value to the dictionary and strip \n character.
            dc[d[0].strip()]=int(d[1].strip())
        myfile.close()
        return dc    

        
    def sample_transcripts(self,avgs, number):
        #extract count numbers
        val=list(avgs.values())
        #calculate relative abundance
        tot=sum(val)
        rel_val=[x/tot for x in val]
        #sampling
        smpl=choices(list(avgs.keys()),weights=rel_val,k=number)
        #initialize empty dictionary
        smpl_dc=dict()
        #count the genes occurence
        for i in smpl:
            if i not in smpl_dc:
                smpl_dc[i]=1
            else:
                smpl_dc[i]+=1
        #return the new dictionary
        return smpl_dc
    
    
    
    def write_sample(self,file,sample):
        #open file in write mode
        myfile=open(file,'w')
        for (k,v) in sample.items():
            #join each key value pair with a blankspace
            ln=' '.join([k,str(v)])
            #write a new line
            myfile.write(ln+'\n')
        myfile.close()