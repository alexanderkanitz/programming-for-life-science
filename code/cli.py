def main():
    from transcript_sampler import TranscriptSampler
    import argparse
    parser = argparse.ArgumentParser(description='Read functions argument')
    parser.add_argument('input', action='store', type=str, help='name of the input file')
    parser.add_argument('n_reads', action='store', type=int, help='number of total reads to be simulated')
    parser.add_argument('output', action='store', type=str, help='name of the output file')
    args = parser.parse_args()
    smplr= TranscriptSampler()
    dc=smplr.read_avg_expression(args.input)
    dc_s=smplr.sample_transcripts(dc, args.n_reads)
    smplr.write_sample(args.output,dc_s)
    
    
if __name__ == '__main__':
    main()